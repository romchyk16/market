<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package market
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function market_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	// Adds a class of no-sidebar when there is no sidebar present.
	if ( ! is_active_sidebar( 'sidebar-1' ) ) {
		$classes[] = 'no-sidebar';
	}

	return $classes;
}
add_filter( 'body_class', 'market_body_classes' );

/**
 * Add a pingback url auto-discovery header for single posts, pages, or attachments.
 */
function market_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'market_pingback_header' );

/**
 * Custom Language Switcher for WPML
 */
function market_switcher_languages($side){

  $html = '';
  $languages = icl_get_languages('skip_missing=1');

  if( 1 < count($languages) ){

  	if ($side == 'header') {
  		$class    = 'text-white';
  		$position = 'dropdownLanguageHeader';
	} else {
		$class    = 'text-gray';
  		$position = 'dropdownLanguageFooter';
	}

    $html .= '<div class="dropdown show">';

    	foreach($languages as $l){

	    	if( $l['active'] ){

	    		$html .= '<a class="dropdown-toggle" href="#" role="button" id="'.$position.'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
	    		
				  $html .= '<span>' . $l['translated_name'] . '</span>
				          </a>';
		    }
	    }

	    $html .= '<div class="dropdown-menu dropdown-menu-right" aria-labelledby="'.$position.'">';

		    foreach($languages as $l){

		    	if( !$l['active'] ){

		    		$html .= '<li>';
					  $html .= '<a class="dropdown-item" href="'.$l['url'].'">
					  				<span>' . $l['translated_name'] . '</span>
						        </a>
					          </li>';
			    }
		    }

    	$html .= '</div>';
    $html .= '</div>';
  }

  return $html;
}