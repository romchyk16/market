<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_post_type('post') 
    ->add_tab( 'Main', array(
        Field::make( "image", "bg_header_single", "Background image (header)")
            ->set_value_type('url')
            ->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
    ));