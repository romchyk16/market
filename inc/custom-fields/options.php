<?php
use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', __( 'Theme Options', 'tenthouse' ) )
    ->set_icon( 'dashicons-hammer' )
    ->set_page_menu_title( 'Setting Template' )
	->add_tab( __('Header'), array(
        Field::make( 'image', 'logo_header', 'Logo' )->set_width( 8 ),
        Field::make( 'text', 'text_logo_header', 'Text logo' )->set_width( 85 ),
        Field::make( 'complex', 'rep_social_home', 'Social' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( 'icon', 'icons_rep_social_home', __( 'Social icons' ) )->set_width( 50 ),
                Field::make( "text", "url_rep_social_home", "Link")->set_width( 50 )->set_default_value( 'https://www.google.ru/' )
            ))
    ))
    ->add_tab( __('Blog slider'), array(
        Field::make( 'image', 'bg_slider_blog', 'Background slider' )
            ->set_value_type( 'url' )->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
        Field::make( 'association', 'items_slider_blog', 'Select articles' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'post',
                )
            ) )
    ))
    /*->add_tab( __('Category slider'), array(
        Field::make( 'image', 'bg_slider_category', 'Background slider' )
            ->set_value_type( 'url' )->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
        Field::make( 'association', 'items_slider_category', 'Select articles' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'post',
                )
            ) )
    ))
    ->add_tab( __('Tag slider'), array(
        Field::make( 'image', 'bg_slider_tag', 'Background slider' )
            ->set_value_type( 'url' )->set_default_value(get_template_directory_uri() . '/img/default-bg-slider.jpg'),
        Field::make( 'association', 'items_slider_tag', 'Select articles' )
            ->set_types( array(
                array(
                    'type' => 'post',
                    'post_type' => 'post',
                )
            ) )
    ))*/
    ->add_tab( __('Footer'), array(
        Field::make( 'text', 'title_menu_1_footer', 'Заглавие меню 1' )->set_width( 20 ),
        Field::make( 'text', 'title_menu_2_footer', 'Заглавие меню 2' )->set_width( 20 ),
        Field::make( 'text', 'title_menu_3_footer', 'Заглавие меню 3' )->set_width( 20 ),
        Field::make( 'text', 'title_menu_4_footer', 'Заглавие меню 4' )->set_width( 20 ),
        Field::make( 'complex', 'rep_social_footer', 'Social' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( 'icon', 'icons_rep_social_footer', __( 'Social icons' ) )->set_width( 50 ),
                Field::make( "text", "url_rep_social_footer", "Link")->set_width( 50 )->set_default_value( 'https://www.google.ru/' )
            )),
        Field::make( 'image', 'logo_footer', 'Logo' )->set_width( 8 ),
        Field::make( 'text', 'text_logo_footer', 'Text logo' )->set_width( 85 ),
        Field::make( 'text', 'copyright_footer', 'Copyright' )->set_width( 70 ),
        Field::make( 'text', 'mail_footer', 'Mail' )->set_width( 30 ),
        Field::make( 'text', 'text_polici_footer', 'Text link 1' )->set_width( 50 ),
        Field::make( 'text', 'polici_footer', 'Link 1' )->set_width( 50 ),
        Field::make( 'text', 'text_terms_footer', 'Text link' )->set_width( 50 ),
        Field::make( 'text', 'terms_footer', 'Link2' )->set_width( 50 ),
        Field::make( 'footer_scripts', 'script_footer', 'Semantic Markup' )->set_width( 100 )
    ));