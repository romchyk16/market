<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

$employees_labels = array(
    'plural_name'   => 'Employees',
    'singular_name' => 'Employee'
);

Container::make('post_meta', 'Custom Data')
	->show_on_template('template-home.php')
	->show_on_post_type('page') 
    ->add_tab( 'Main', array(
        Field::make( "text", "title_main_home", "Title"),
        Field::make( "text", "subtitle_main_home", "Subtitle"),
        Field::make( "text", "text_btn_left_main_home", "Text button left")->set_width( 50 ),
        Field::make( "text", "link_btn_left_main_home", "Link button left")->set_width( 50 ),
        Field::make( "text", "text_btn_right_main_home", "Text button right")->set_width( 50 ),
        Field::make( "text", "link_btn_right_main_home", "Link button right")->set_width( 50 ),
        Field::make( "text", "pulsating_circle_main_home", "Pulsating circle")->set_width( 100 ),
        Field::make( "textarea", "text_left_main_home", "Text block left")->set_width( 33.3 ),
        Field::make( "textarea", "text_right_top_main_home", "Text block right top")->set_width( 33.3 ),
        Field::make( "textarea", "text_right_bottom_main_home", "Text block right bottom")->set_width( 33.3 ),
        Field::make( "image", "img_left_main_home", "Image block left")->set_width( 33.3 ),
        Field::make( "image", "img_right_top_main_home", "Image block right top")->set_width( 33.3 ),
        Field::make( "image", "img_right_bottom_main_home", "Image block right bottom")->set_width( 33.3 ),
        Field::make( "image", "img_before_main_home", "Image before")->set_width( 50 ),
        Field::make( "image", "img_after_main_home", "Image after")->set_width( 50 )
    ))
    ->add_tab( 'Trusted', array(
        Field::make( 'media_gallery', 'gallery_trusted_home', 'Gallery' )->set_type( array( 'image' ) ),
        Field::make( "text", "title_trusted_home", "Text")
    ))
	->add_tab( 'Benefits', array(
		Field::make( 'complex', 'rep_benefits_home', 'Bloks' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( "text", "title_rep_benefits_home", "Title")->set_width( 50 ),
                Field::make( "textarea", "desc_rep_benefits_home", "Description")->set_width( 50 ),
                Field::make( "image", "img_rep_benefits_home", "Image")->set_width( 8 ),
                Field::make( "text", "url_rep_benefits_home", "Link")->set_width( 40 ),
                Field::make( "text", "text_url_rep_benefits_home", "Text link")->set_width( 40 )
            ))
	))
    ->add_tab( 'Exchange', array(
        Field::make( "image", "bg_exchange_home", "Background image")->set_width( 8 ),
        Field::make( "text", "title_exchange_home", "Title")->set_width( 80 )
    ))
    ->add_tab( 'Team', array(
        Field::make( "text", "title_team_home", "Title")->set_width( 100 ),
        Field::make( 'complex', 'rep_team_home', 'Bloks' )->set_collapsed( true )->setup_labels($employees_labels)
            ->add_fields( array(
                Field::make( "image", "img_rep_team_home", "Image")->set_width( 8 ),
                Field::make( "text", "name_url_rep_team_home", "Full name")->set_width( 10 ),
                Field::make( "text", "job_rep_team_home", "Job")->set_width( 10 ),
                Field::make( "text", "url_rep_team_home", "Link linkedin")->set_width( 20 ),
                Field::make( "image", "logo_rep_team_home", "Logo")->set_width( 10 ),
                Field::make( "oembed", "yt_rep_team_home", "Link YouTube")->set_width( 10 ),
                Field::make( "textarea", "desc_rep_team_home", "Description")->set_width( 20 )
            )),
        /*Field::make( "text", "text_btn_team_home", "Text button")->set_width( 50 ),
        Field::make( "text", "url_btn_team_home", "Link button")->set_width( 50 )*/
    ))
    ->add_tab( 'Videos', array(
        Field::make( "text", "title_videos_home", "Title")->set_width( 100 ),
        Field::make( 'complex', 'rep_videos_home', 'Tabs' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( "text", "title_rep_videos_home", "Title")->set_width( 100 ),
                Field::make( 'complex', 'rep_rep_videos_home', 'Slides' )->set_collapsed( true )
                    ->add_fields( array(
                        Field::make( "text", "title_rep_rep_videos_home", "Title")->set_width( 50 ),
                        Field::make( "textarea", "desc_rep_rep_videos_home", "Description")->set_width( 50 ),
                        Field::make( "date_time", "date_rep_rep_videos_home", "Date")->set_input_format('d.m.Y H:i', 'd.m.Y H:i')->set_width( 33.3 ),
                        Field::make( "text", "country_rep_rep_videos_home", "Country")->set_width( 33.3 ),
                        Field::make( 'oembed', 'video_rep_rep_videos_home', 'Url Video' )
                    ))
            ))
    ))
    ->add_tab( 'Books', array(
        Field::make( "text", "title_books_home", "Title")->set_width( 100 ),
        Field::make( 'complex', 'rep_books_home', 'Books' )->set_collapsed( true )
            ->add_fields( array(
                Field::make( "image", "img_rep_books_home", "Image")->set_width( 8 ),
                Field::make( "text", "title_rep_books_home", "Title")->set_width( 80 ),
                Field::make( 'complex', 'rep_rep_books_home', 'Languages' )
                    ->add_fields( array(
                        Field::make( "text", "title_rep_rep_books_home", "Title")->set_width( 50 ),
                        Field::make( "text", "link_rep_rep_books_home", "Link")->set_width( 50 )
                    ))
            ))
	));