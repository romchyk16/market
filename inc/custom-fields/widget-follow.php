<?php 

use Carbon_Fields\Widget;
use Carbon_Fields\Field;

class Follow_Widget extends Widget {
    // Register widget function. Must have the same name as the class
    function __construct() {
        $this->setup( 'theme_widget_example', 'Follow us', 'Displays a block with title/text', array(
            Field::make( "text", "title_follow", "Social link"),
            Field::make( 'complex', 'rep_follow', 'Social' )
                ->add_fields( array(
                    Field::make( "text", "url_rep_follow", "Social link"),
                    Field::make( 'color', 'bg_rep_follow', 'Background color' ),
                    Field::make( 'icon', 'icons_rep_follow', 'Social icons' )
                ))
        ) );
    }

    // Called when rendering the widget in the front-end
    function front_end( $args, $instance ) { ?>

        <div class="blog-follow">
            <div><?php echo $instance['title_follow']; ?></div>
            <ul class="blog-follow-social">

            <?php foreach((array)$instance['rep_follow'] as $item): ?>
                <li class="link-facebook" style="background: <?php print_r( $item['bg_rep_follow'] ); ?>;">
                    <a target="_blank" href="<?php echo $item['url_rep_follow']; ?>">
                        <i class="<?php echo $item['icons_rep_follow']['class']; ?>"></i>
                    </a>
                </li>
            <?php endforeach; ?>

            </ul>
        </div>

    <?php }
}

// function load_widgets() {
//     register_widget( 'Follow_Widget' );
// }