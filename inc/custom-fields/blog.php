<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make('post_meta', 'Custom Data')
	->show_on_template('template-blog.php')
	->show_on_post_type('page') 
    ->add_tab( 'Features', array(
        Field::make( "image", "bg_features", "Background image")->set_width( 8 ),
        Field::make( "text", "title_features", "Title")->set_width( 88 ),
        Field::make( "textarea", "desc_features", "Description")->set_width( 100 ),
        Field::make( "text", "text_btn_l_features", "Text button (left)")->set_width( 50 ),
        Field::make( "text", "url_btl_l_features", "Link button (left)")->set_width( 50 ),
        Field::make( "text", "text_btn_r_features", "Text button (right)")->set_width( 50 ),
        Field::make( "text", "url_btl_r_features", "Link button (right)")->set_width( 50 )
	));