<?php
/**
 * Template name: Blog
 */

get_header(); ?>

<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post();

        $count = 1;
        $args = array(
            'posts_per_page'    => 15,
            'post_type'         => 'post',
            'post_status'       => 'publish',
            'suppress_filters'  => false
        );

        $query = new WP_Query($args);
        if ($query->found_posts <= 15) {
            $class_btn = 'hide-btn';
        } else {
            $class_btn = '';
        } ?>

        <!-- Blog section -->
        <section class="blog">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-width">
                        <div class="blog-head head-item-line">
                            <?php market_breadcrumbs(); ?>
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="blog-item-container">
                    <div class="blog-item-wrapper">

                        <?php if ($query->have_posts()) {
                            while ($query->have_posts()) {
                                $query->the_post();

                                get_template_part('template-parts/blog', 'loop');

                            }
                            wp_reset_postdata();
                        } ?>

                    </div>

                    <script>
                        found_posts = '<?php echo $query->found_posts; ?>';
                    </script>

                    <div class="load-more-wrapper <?php echo $class_btn; ?>">
                        <div class="load-more"><?php echo __('Load more', 'market'); ?></div>
                    </div>

                    <div class="sidebar-container">
                        <?php get_template_part('template-parts/sidebar'); ?>
                    </div>
                    <!-- /.sidebar-container -->
                </div>
                <!-- /.blog-item-container -->
            </div>
            <!-- /.container -->

        </section>

        <?php get_template_part('template-parts/blog/features'); ?>

    <?php endwhile; ?>

<?php endif; ?>

<?php get_footer();