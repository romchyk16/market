<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package market
 */

get_header(); ?>

<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post();

        if (empty(get_post_meta($post->ID, 'views', true))) {
            $views = '0';
        } else {
            $views = get_post_meta($post->ID, 'views', true);
        }
        if (empty(get_post_meta($post->ID, 'likes', true))) {
            $like = '0';
        } else {
            $like = get_post_meta($post->ID, 'likes', true);
        }
        $comments_count = wp_count_comments($post->ID);
        $term_list = wp_get_post_terms($post->ID, 'post_tag', array("fields" => "all"));
        $categories = wp_get_post_categories($post->ID, array('fields' => 'all')); ?>

        <!-- Blog section -->
        <section class="blog single-entry"> 
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-width">
                        <div class="blog-head">
                            <?php market_breadcrumbs(); ?>
                            <?php get_search_form(); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="blog-item-container">
                    <div class="blog-item-wrapper">
                        <div class="single-blog-item">

                            <div class="blog-category-info">
                                <?php foreach ((array)$categories as $category) { ?>
                                    <a href="<?php echo get_tag_link($category->term_id); ?>"
                                       class="blog-item-big-categories">
                                        <?php echo $category->name; ?>
                                    </a>
                                <?php } ?>
                                <span class="blog-item-big-date">
                                    <?php echo get_the_date('M d, Y'); ?>
                                </span>
                            </div>

                            <h1><?php the_title(); ?></h1>
                            <div class="single-blog-item-statistic">
                                <div class="blog-item-big-statistic">
                                    <a class="icon-eye"><?php echo $views ?></a>
                                    <a href="#comments"
                                       class="icon-chat-empty scrollto"><?php echo $comments_count->approved; ?></a>
                                    <a class="icon-thumbs-up-alt lp-like" id="lk<?php echo $post->ID; ?>"
                                       data-like="<?php echo $post->ID; ?>">
                                        <?php echo $like; ?>
                                    </a>

                                </div>
                                <!-- /.blog-item-big-statistic -->
                                <?php //echo do_shortcode('[social_sharing]'); ?>
                                <?php echo do_shortcode('[Sassy_Social_Share count="1"]'); ?>
                                <!-- <div class="social"> -->

                                <!-- /.share-article -->
                            </div>
                            <!-- /.single-blog-item-statistic -->
                        </div>
                        <!-- /.single-blog-item -->

                        <div class="single-blog-entry head-item">
                            <div class="blog-entry-image">
                                <?php if (has_post_thumbnail()):
                                    $image_alt = get_post_meta($post->ID, '_wp_attachment_image_alt', true); ?>
                                    <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"
                                         alt="<?php echo $image_alt; ?>">
                                <?php endif; ?>
                            </div>
                            <!-- /.blog-entry-image -->
                            <?php the_content(); ?>

                            <ul class="blog-tags">
                                <?php foreach ((array)$term_list as $term): ?>
                                    <li>
                                        <a href="<?php echo get_tag_link($term->term_id); ?>"><?php echo $term->name; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <!-- /.blog-tags -->
                        </div>
                        <!-- /.single-blog-entry -->

                        <?php get_template_part('template-parts/blog/resent', 'post'); ?>

                        <div class="write-comment-lg">
                            <?php comments_template(); ?>
                        </div>

                    </div>
                    <div class="sidebar-container">
                        <?php get_template_part('template-parts/sidebar'); ?>
                    </div>
                </div>
                <!-- /.blog-item-container -->
            </div>
        </section>

    <?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); 