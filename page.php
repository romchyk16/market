<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package market
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>  

	<?php while ( have_posts() ) : the_post(); ?>

		<!-- Blog section -->
		<section class="blog single-entry">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-width">
						<div class="blog-head">
							<?php market_breadcrumbs(); ?>
							<?php get_search_form(); ?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9">
                       <!--  <span class="blog-item-big-date">
                            <?php //echo get_the_date('M d, Y'); ?>
                        </span> -->
                        <!-- /.blog-item-big-date -->

                        <div class="single-blog-item">
                            <!-- <h1><?php //the_title(); ?></h1> -->
                            <!-- <div class="single-blog-item-statistic">  -->
                                <!-- <div class="blog-item-big-statistic">
                                    <a class="icon-eye"><?php //echo $views ?></a>
                                    <a href="#comments" class="icon-chat-empty scrollto"><?php //echo $comments_count->approved; ?></a>
                                    <a class="icon-thumbs-up-alt lp-like" id="lk<?php //echo $post->ID; ?>" data-like="<?php //echo $post->ID; ?>">
                                        <?php //echo $like; ?>
                                    </a>

                                </div> -->
                                <!-- /.blog-item-big-statistic -->
                                <?php //echo do_shortcode('[social_sharing]'); ?>
                                <!-- /.share-article -->
                            <!-- </div> -->
                            <!-- /.single-blog-item-statistic -->
                        </div>
                        <!-- /.single-blog-item -->

                        <div class="single-blog-entry head-item">
                        	<?php if(has_post_thumbnail()): ?>
                            	<div class="blog-entry-image">
                                    <?php $image_alt = get_post_meta( $post->ID, '_wp_attachment_image_alt', true); ?>
                                    <img src="<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>" alt="<?php echo $image_alt; ?>">
                           		</div>
                       		<?php endif; ?>
                            <!-- /.blog-entry-image -->
                            <?php the_content(); ?>
                            <!-- /.blog-tags -->
                        </div>
                        <!-- /.single-blog-entry -->

                        <?php //get_template_part('template-parts/blog/resent', 'post'); ?>

                        <?php //comments_template(); ?>

					</div>
					<div class="col-lg-3 col-md-12"> 
						<?php get_template_part('template-parts/sidebar'); ?>
					</div>
				</div>
			</div>
		</section>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer();