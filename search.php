<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package market
 */

get_header(); ?>

	<!-- Blog section -->
	<section class="blog">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-width">
					<div class="blog-head">
						<?php market_breadcrumbs(); ?>
						<?php get_search_form(); ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-9">
                   
					<?php if ( have_posts() ) : ?>

						<header class="page-header">
							<h1 class="page-title">
								<?php
								/* translators: %s: search query. */
								printf( esc_html__( 'Search Results for: %s', 'market' ), '<span>' . get_search_query() . '</span>' );
								?>
							</h1>
						</header><!-- .page-header -->

	 					<div class="blog-item-wrapper">
	 						
							<?php while ( have_posts() ) : the_post();

								get_template_part('template-parts/blog', 'loop');

							endwhile; ?>

						</div>

					<?php else : ?>

						<header class="page-header">
							<h1 class="page-title">
								<?php echo __( 'Nothing found on your request', 'market' ); ?>
							</h1>
						</header>
						<div class="blog-item-wrapper"></div>

					<?php endif; ?>

                </div>
				<div class="col-md-3">
					<?php get_template_part('template-parts/sidebar'); ?>
				</div>
			</div>
		</div>
	</section>

<?php get_footer();