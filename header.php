<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Market
 */
?>

    <!doctype html>
    <html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="180x180"
              href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32"
              href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16"
              href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
        <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/favicon/safari-pinned-tab.svg"
              color="#b31f7a">
        <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.ico">
        <meta name="msapplication-config"
              content="<?php echo get_template_directory_uri(); ?>/favicon/browserconfig.xml">
        <meta name="theme-color" content="#ffffff">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">

        <?php if (is_page_template('template-home.php')) { ?>
            <?php if ( function_exists('icl_object_id') ) { ?>
                <meta property="og:locale" content="<?php echo ICL_LANGUAGE_CODE; ?>">
            <?php } else { ?>
                <meta property="og:locale" content="en">
            <?php } ?>
            <meta property="og:type" content="website">
            <meta property="og:title" content="SocialMedia.Market - Influencer Marketing Platform">
            <meta property="og:description" content="The first decentralized platform connecting social media bloggers and advertisers. Monetize your Youtube, Instagram, Facebook, Twitter ">
            <meta property="og:url" content="https://socialmedia.market">
            <meta property="og:site_name" content="SocialMedia.Market">
            <meta property="og:image" content="https://socialmedia.market/assets/img/smm_preview.jpg">
            <meta property="og:image:width" content="1200">
            <meta property="og:image:height" content="1200">
        <?php } ?>

        <?php wp_head(); ?>
    </head>

<body <?php body_class(); ?>>

    <header class="section-header">
        <div class="container">
            <div class="row">
                <?php get_template_part('template-parts/tokens'); ?>
            </div>
        </div>
    </header>

    <style>
        .header-logo .st0,
        .header-logo .st1,
        .header-logo .st2,
        .header-logo .st3 {
            fill: #fff!important;
        }
    </style>

<?php if (!is_page_template('template-home.php')) { ?>

    <div class="mnu hidden-lg hidden-md hidden-sm">

        <?php //get_template_part('template-parts/tokens'); ?>

        <nav class="header-nav">
            <?php if (has_nav_menu('header_1')) {
                wp_nav_menu(array(
                    'theme_location' => 'header_1',
                    'container' => false,
                    'items_wrap' => '<ul class="list-unstyled">%3$s</ul>',
                ));
            } ?>
        </nav>
        <?php echo market_switcher_languages('header'); ?>
    </div>

    <?php /*if (is_category()) {
        
        if (!empty(carbon_get_theme_option('bg_slider_category'))) {
            $bg_url = carbon_get_theme_option('bg_slider_category');
        } else {
            $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
        }
    } elseif (is_tag()){
        
        if (!empty(carbon_get_theme_option('bg_slider_tag'))) {
            $bg_url = carbon_get_theme_option('bg_slider_tag');
        } else {
            $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
        }
    } else*/if (is_page_template('template-blog.php') || is_archive()){
        
        if (!empty(carbon_get_theme_option('bg_slider_blog'))) {
            $bg_url = carbon_get_theme_option('bg_slider_blog');
        } else {
            $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
        }
    } elseif (is_single()){
        
        if (!empty(carbon_get_the_post_meta('bg_header_single'))) {
            $bg_url = carbon_get_the_post_meta('bg_header_single');
        } else {
            $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
        }
    } else {
        $bg_url = get_template_directory_uri() . '/img/default-bg-slider.jpg';
    } ?>

    <!-- Header -->
    <section class="header">
        <div class="header-main-top header-main"
             style="background: url(<?php echo $bg_url; ?>) no-repeat top;">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xl-3">
                        <a href="#" class="toggle-nav"><span></span></a>
                        <a href="<?php echo esc_url(home_url('/')); ?>" class="header-logo"> 
                            <?php $svg_file = file_get_contents( wp_get_attachment_image_url(carbon_get_theme_option('logo_header'), 'full') ); ?>
                            <?php echo $svg_file; ?>
                        </a>
                    </div>
                    <div class="col-12 col-xl-9">
                        <div class="hidden-xs">
                            <div class="header-nav-wrapper">
                                <nav class="header-nav">
                                    <?php if (has_nav_menu('header_1')) {
                                        wp_nav_menu(array(
                                            'theme_location' => 'header_1',
                                            'container' => false,
                                            'items_wrap' => '<ul class="list-unstyled">%3$s</ul>',
                                        ));
                                    } ?>

                                    <?php echo market_switcher_languages('header'); ?>

                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">

                        <?php /*if (is_category()) {
                            get_template_part('template-parts/blog/slider', 'category');
                        } elseif (is_tag()){
                            get_template_part('template-parts/blog/slider', 'tag');
                        } else*/if (is_page_template('template-blog.php') || is_archive()){
                            get_template_part('template-parts/blog/slider', 'blog');
                        } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
<?php } ?>