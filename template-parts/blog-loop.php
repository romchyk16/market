<?php
global $count;
if (empty(get_post_meta($post->ID, 'views', true))) {
    $views = 0;
} else {
    $views = get_post_meta($post->ID, 'views', true);
}
if (empty(get_post_meta($post->ID, 'likes', true))) {
    $like = '0';
} else {
    $like = get_post_meta($post->ID, 'likes', true);
}
$comments_count = wp_count_comments($post->ID);
$categories = wp_get_post_categories($post->ID, array('fields' => 'all')); ?>

<?php if ($count % 3 == 0) { ?>

    <div <?php post_class('blog-item blog-item-big'); ?> data-post-id="<?php echo $post->ID; ?>">
        <?php if (has_post_thumbnail()):
            $image_alt = get_post_meta($post->ID, '_wp_attachment_image_alt', true); ?>
            <div class="item-photo-wrap">
                <a href="<?php the_permalink(); ?>" class="item-photo scale-image">
                    <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"
                         alt="<?php echo $image_alt; ?>">
                </a>
                <!-- /.item-photo -->
            </div>
        <?php endif; ?>
        <div class="blog-item-big-text">
            <?php foreach ((array)$categories as $category) { ?>
                <a href="<?php echo get_category_link($category->term_id); ?>" class="blog-item-big-categories">
                    <?php echo $category->name; ?>
                </a>
            <?php } ?>
            <a href="<?php the_permalink(); ?>" class="blog-item-big-name"><?php the_title(); ?></a>
            <div class="blog-item-big-info">
                <div class="blog-item-big-date"><?php echo get_the_date('M d, Y'); ?></div>
                <div class="blog-item-big-statistic">
                    <span class="blog-item-big-views icon-eye"><a><?php echo $views ?></a></span>
                    <span><a class="icon-chat-empty" href="<?php the_permalink(); ?>#comments"
                             class="blog-item-big-comments">
                            <?php echo $comments_count->approved; ?></a>
                    </span>
                    <span class="blog-item-big-likes icon-thumbs-up-alt"><a><?php echo $like; ?></a></span>
                </div>
            </div>
        </div>
    </div>

<?php } else { ?>

    <div <?php post_class('blog-item'); ?> data-post-id="<?php echo $post->ID; ?>">
        <?php if (has_post_thumbnail()):
            $image_alt = get_post_meta($post->ID, '_wp_attachment_image_alt', true); ?>
            <div class="item-photo-wrap">
                <a href="<?php the_permalink(); ?>" class="item-photo scale-image">
                    <img src="<?php echo get_the_post_thumbnail_url($post->ID, 'full'); ?>"
                         alt="<?php echo $image_alt; ?>">
                </a>
                <!-- /.item-photo -->
            </div>
            <!-- /.item-photo-wrap -->
        <?php endif; ?>
        <div class="blog-item-text-wrap">
            <div class="blog-item-text">
                <div class="blog-item-info">
                    <?php foreach ((array)$categories as $category) { ?>
                        <a href="<?php echo get_category_link($category->term_id); ?>"
                           class="blog-item-categories">
                            <?php echo $category->name; ?>
                        </a>
                    <?php } ?>
                    <div class="blog-item-date"><?php echo get_the_date('M d,  Y'); ?></div>
                    <a href="<?php the_permalink(); ?>" class="blog-item-name"><?php the_title(); ?></a>
                </div>
            </div>
        </div>
    </div>

<?php }
++$count; ?>