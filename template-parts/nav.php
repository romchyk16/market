<div class="mnu hidden-lg hidden-md hidden-sm">

    <?php get_template_part('template-parts/tokens'); ?>

    <nav class="header-nav">
        <?php if (has_nav_menu('header_1')) {
            wp_nav_menu(array(
                'theme_location' => 'header_1',
                'container' => false,
                'items_wrap' => '<ul class="list-unstyled">%3$s</ul>',
            ));
        } ?>
    </nav>
    <?php echo market_switcher_languages('header'); ?>
</div>

<section class="header-main-top home-header-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-xl-3">
                <a href="#" class="toggle-nav"><span></span></a>
                <a href="<?php echo esc_url(home_url('/')); ?>" class="header-logo">
                    <?php $svg_file = file_get_contents( wp_get_attachment_image_url(carbon_get_theme_option('logo_header'), 'full') ); ?>
                    <?php echo $svg_file; ?>
                </a>
            </div>
            <div class="col-12 col-xl-9">
                <div class="hidden-xs">
                    <div class="header-nav-wrapper">
                        <nav class="header-nav">
                            <?php if (has_nav_menu('header_1')) {
                                wp_nav_menu(array(
                                    'theme_location' => 'header_1',
                                    'container' => false,
                                    'items_wrap' => '<ul class="list-unstyled">%3$s</ul>',
                                ));
                            } ?>
                            <?php echo market_switcher_languages('header'); ?>

                            <div class="header-tooltip">
                                <div class="head-tooltip">
                                    <p>CHOOSE SOLUTION</p>
                                    <p>PRODUCT FEATURES</p>
                                </div>
                                <!-- /.head-tooltip -->

                                <div class="content-tooltip-wrap">
                                    <div class="content-tooltip content-tooltip-left">
                                        <a href="#" class="content-tooltip-item icon-star-filled">
                                            <p>For influencers</p>
                                            <span>monetize you channel easily</span>
                                        </a>
                                        <a href="#" class="content-tooltip-item icon-volume-off">
                                            <p>For advertisers</p>
                                            <span>make your brand known</span>
                                        </a>
                                    </div>

                                    <div class="content-tooltip content-tooltip-right">
                                        <a href="#" class="content-tooltip-item icon-blank">
                                            <p>SMT token</p>
                                            <span>find unlimited opportunities</span>
                                        </a>
                                    </div>
                                </div>
                                <!-- /.content-tooltip-wrap -->

                            </div>
                            <!-- /.header-tooltip -->

                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>