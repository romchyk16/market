<!-- Section: Videos -->
<?php $blocks = carbon_get_post_meta($post->ID, 'rep_videos_home'); ?>
<section class="section-videos">
    <div class="container">
        <div class="section-video-tabs">
            <h2 class="section-title text-center mb-5 font-weight-normal"><?php echo carbon_get_post_meta($post->ID, 'title_videos_home'); ?></h2>
            <ul class="videos-list">

                <?php foreach ((array)$blocks as $block): ?>

                    <?php $items = $block['rep_rep_videos_home']; ?>

                    <li class="videos-list--item">

                        <?php foreach ((array)$items as $key => $item):

                            parse_str(parse_url($item['video_rep_rep_videos_home'], PHP_URL_QUERY), $youtube);

                            $data[$key] = array(
                                'title' => $item['title_rep_rep_videos_home'],
                                'desc' => $item['desc_rep_rep_videos_home'],
                                'date' => $item['date_rep_rep_videos_home'], //'02.02.2018 22:35'
                                'country' => $item['country_rep_rep_videos_home'],
                                'video' => $item['video_rep_rep_videos_home'],
                                'thumb' => 'https://img.youtube.com/vi/' . $youtube['v'] . '/mqdefault.jpg',
                                'share' => $item['video_rep_rep_videos_home']
                            );

                        endforeach;
                        $data_json = json_encode($data);
                        ?>

                        <a href="#" class="videos-list--item-link" data-video-list='<?php echo $data_json; ?>'>
                            <span><?php echo $block['title_rep_videos_home']; ?></span>
                            <i class="fa fa-chevron-right" aria-hidden="true"></i>
                        </a>
                    </li>

                <?php endforeach; ?>

            </ul>

            <div class="card-video">
                <div class="card-video--content">
                    <div id="video-fotorama" class="fotorama">
                        <!-- Loads dynamically -->
                    </div>
                </div>
                <div class="card-video--footer">
                    <div class="video-info">
                        <div class="media">
                            <div class="media-body">
                                <span><i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;<span id="card-video-date"></span></span>
                                | <span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;<span
                                            id="card-video-country"></span></span>
                            </div>
                            <div class="media-element fr">
                                <a href="#" target="_blank" id="card-video-share">
                                    <i class="fa fa-share" aria-hidden="true"></i>&nbsp;<span><?php echo __('Share video', 'market'); ?></span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <h4 id="card-video-title"></h4>
                    <p id="card-video-desc"></p>
                </div>
            </div>
        </div>
        <!-- /.section-video-tabs -->
    </div>
    <!-- /.container -->
</section>