<!-- Section: Benefits -->
<section class="section-benefits py-5">
	<div class="container">

		<?php
	    $count  = 1;
	    $blocks = carbon_get_post_meta( $post->ID, 'rep_benefits_home' );
	    ?>

		<?php foreach( (array)$blocks as $block ): ?>

			<?php if( !empty($block['img_rep_benefits_home'])   ||
				      !empty($block['title_rep_benefits_home']) ||
				      !empty($block['desc_rep_benefits_home'])  ||
				      !empty($block['url_rep_benefits_home'])   ||
				      !empty($block['text_url_rep_benefits_home']) ): ?>

				<!-- Benefit #<?php echo $count; ?> -->
				<?php if( $count % 2 != 0 ): ?>
					<div class="benefits-item row py-2 align-items-center flex-column flex-sm-row mb-5 mb-md-0">
				<?php else: ?>
					<div class="benefits-item row py-2 align-items-center flex-column flex-sm-row-reverse mb-5 mb-md-0">
				<?php endif; ?>
					    <div class="col-md-6 text-center mb-4 mb-md-0">
					    	<?php if(!empty($block['img_rep_benefits_home'])):
					    		$image_alt = get_post_meta( $block['img_rep_benefits_home'], '_wp_attachment_image_alt', true); ?>
                                <div class="benefits-item-wrap benefits-item-wrap<?php echo $count ?>">
                                    <img class="lazyload" data-src="<?php echo wp_get_attachment_image_url( $block['img_rep_benefits_home'], 'full' ); ?>" width="456" height="472" alt="<?php echo $image_alt; ?>" class="img-fluid">
                                </div>
                                <!-- /.benefits-item-wrap -->
				      		<?php endif; ?>
					    </div>
					    <div class="col-md-6">
					      <h2 class="benefits-item--title mt-0 mb-3 text-social-media font-weight-normal"><?php echo $block['title_rep_benefits_home']; ?></h2>
					      <p class="benefits-item--desc font-weight-light"><?php echo $block['desc_rep_benefits_home']; ?></p>
					      <?php if(!empty($block['url_rep_benefits_home'])): ?>
						      <div>
						        <a href="<?php echo $block['url_rep_benefits_home']; ?>" class="btn btn-lg btn-social-media px-4">
						        	<span><?php echo $block['text_url_rep_benefits_home']; ?></span>
						        </a>
						      </div>
					      <?php endif; ?>
					    </div>
					</div>
				<?php endif; ?>

			<?php ++$count; ?>
        <?php endforeach; ?>

	</div>
</section>