<!-- Section: Books -->
<?php $blocks = carbon_get_post_meta( $post->ID, 'rep_books_home' );  ?>
<div class="footer-bg">
<section class="section-books py-5">
    <div class="container">
      <div class="row">
        <div class="col-xl-6">
          <h2 class="text-center text-md-left mb-5 text-social-media font-weight-normal"><?php echo carbon_get_post_meta( $post->ID, 'title_books_home' ); ?></h2>
        </div>
      </div>
      <div class="row">

        <?php foreach( (array)$blocks as $block ): ?>
          
          <?php $items = $block['rep_rep_books_home']; ?>

          <div class="col-12 col-sm-12 offset-sm-0 col-md-6 col-lg-3">
            <div class="book-item bg-white p-3 mb-5 mb-lg-0" data-mh="book-item">
              <div class="media">
                <div class="media-body">
                  <h5 class="book-item--title font-weight-normal mb-3"><?php echo $block['title_rep_books_home']; ?></h5>
                  <div class="row no-gutters book-item--language">

                    <?php foreach( (array)$items as $key => $item ): ?>

                      <?php if(!empty($item['link_rep_rep_books_home']) || !empty($item['title_rep_rep_books_home'])): ?>

                        <div class="col-6 col-sm-6 col-lg-12 col-xl-6 px-1">
                          <a href="<?php echo $item['link_rep_rep_books_home']; ?>" class="btn btn-sm btn-block mb-2" role="button" aria-pressed="true">
                            <?php echo $item['title_rep_rep_books_home']; ?>
                          </a>
                        </div>

                      <?php endif; ?>

                    <?php endforeach; ?>

                  </div>
                </div>
                <div class="book-item--image">
                  <?php if(!empty($block['img_rep_books_home'])): 
                    $image_alt = get_post_meta( $block['img_rep_books_home'], '_wp_attachment_image_alt', true); ?>
                    <img class="lazyload" data-src="<?php echo wp_get_attachment_image_url( $block['img_rep_books_home'], 'full' ); ?>" width="127" height="180" alt="<?php echo $image_alt; ?>">
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>

        <?php endforeach; ?>

      </div>
    </div>
</section>