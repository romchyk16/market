<!-- Section: Main -->
<section class="section-main">
    <div class="jumbotron jumbotron-fluid text-center m-0 pb-3">
      <div class="container">
        <h1 class="display-5 font-weight-normal mt-5 mb-3"><?php echo carbon_get_post_meta( $post->ID, 'title_main_home' ); ?></h1>
        <p class="lead mb-5"><?php echo carbon_get_post_meta( $post->ID, 'subtitle_main_home' ); ?></p>
        <div class="mb-5">
          <a class="btn text-uppercase btn-primary-influencer mr-0 mr-md-5 mb-4 mb-md-0" href="<?php echo carbon_get_post_meta( $post->ID, 'link_btn_left_main_home' ); ?>" role="button">
				<?php echo carbon_get_post_meta( $post->ID, 'text_btn_left_main_home' ); ?>
          </a>
          <a class="btn text-uppercase btn-primary-advertiser mb-4 mb-md-0" href="<?php echo carbon_get_post_meta( $post->ID, 'link_btn_right_main_home' ); ?>" role="button">
          		<?php echo carbon_get_post_meta( $post->ID, 'text_btn_right_main_home' ); ?>
          </a>
        </div>

        <!-- Pulsating Circle -->
        <div class="pulsating-circle-wrapper mb-5 d-none d-md-block">
          <div class="pulsating-circle">
            <span><?php echo carbon_get_post_meta( $post->ID, 'pulsating_circle_main_home' ); ?></span>
          </div>
        </div>

        <div class="position-relative">
          
        <?php if( carbon_get_post_meta( $post->ID, 'text_right_top_main_home' ) ): ?>
          <div class="comparison-reason comparison-reason1 text-right d-none d-xl-block">
            <p class="font-weight-light text-white"><?php echo carbon_get_post_meta( $post->ID, 'text_right_top_main_home' ); ?></p>
            <div>
              <i class="fa fa-long-arrow-left" aria-hidden="true" style="color: #00ffff;"></i>
            </div>
          </div>
        <?php endif; ?>

        <?php if( carbon_get_post_meta( $post->ID, 'text_left_main_home' ) ): ?>
          <div class="comparison-reason comparison-reason2 text-left d-none d-xl-block">
            <p class="font-weight-light text-white"><?php echo carbon_get_post_meta( $post->ID, 'text_left_main_home' ); ?></p>
            <div>
              <i class="fa fa-long-arrow-right" aria-hidden="true" style="color: #00ffff;"></i>
            </div>
          </div>
        <?php endif; ?>

        <?php if( carbon_get_post_meta( $post->ID, 'text_right_bottom_main_home' ) ): ?>
          <div class="comparison-reason comparison-reason3 text-right d-none d-xl-block">
            <p class="font-weight-light text-black"><?php echo carbon_get_post_meta( $post->ID, 'text_right_bottom_main_home' ); ?></p>
            <div>
              <i class="fa fa-long-arrow-left" aria-hidden="true" style="color: #f74779;"></i>
            </div>
          </div>
        <?php endif; ?>

          <!-- Comparison Slider start -->
          <div class="comparison-wrapper mx-auto my-0">
			<?php if(!empty(carbon_get_post_meta( $post->ID, 'img_left_main_home' ))): 
              $image_alt = get_post_meta( carbon_get_post_meta( $post->ID, 'img_left_main_home' ), '_wp_attachment_image_alt', true); ?>
            	<img class="laptop-image1 lazyload" data-src="<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'img_left_main_home' ), 'full' ); ?>" alt="<?php echo $image_alt ?>">
        	<?php endif; ?>
			<?php if(!empty(carbon_get_post_meta( $post->ID, 'img_right_top_main_home' ))): 
              $image_alt = get_post_meta( carbon_get_post_meta( $post->ID, 'img_right_top_main_home' ), '_wp_attachment_image_alt', true); ?>
            	<img class="laptop-image2 lazyload" data-src="<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'img_right_top_main_home' ), 'full' ); ?>" alt="<?php echo $image_alt; ?>">
        	<?php endif; ?>
			<?php if(!empty(carbon_get_post_meta( $post->ID, 'img_right_bottom_main_home' ))): 
              $image_alt = get_post_meta( carbon_get_post_meta( $post->ID, 'img_right_bottom_main_home' ), '_wp_attachment_image_alt', true); ?>
            	<img class="laptop-image3 lazyload" data-src="<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'img_right_bottom_main_home' ), 'full' ); ?>" alt="<?php echo $image_alt; ?>">
        	<?php endif; ?>

            <figure class="cd-image-container">
            	<?php if(!empty(carbon_get_post_meta( $post->ID, 'img_before_main_home' ))): 
                $image_alt = get_post_meta( carbon_get_post_meta( $post->ID, 'img_before_main_home' ), '_wp_attachment_image_alt', true); ?>
	            	<img class="lazyload" data-src="<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'img_before_main_home' ), 'full' ); ?>" width="720" height="440" alt="<?php echo $image_alt; ?>">
	        	<?php endif; ?>
              	<span class="cd-image-label" data-type="original">Original</span>

              <div class="cd-resize-img"><!-- the resizable image on top -->
              	<?php if(!empty(carbon_get_post_meta( $post->ID, 'img_after_main_home' ))): 
                  $image_alt = get_post_meta( carbon_get_post_meta( $post->ID, 'img_after_main_home' ), '_wp_attachment_image_alt', true); ?>
    	            	<img class="lazyload" data-src="<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'img_after_main_home' ), 'full' ); ?>" width="720" height="440" alt="<?php echo $image_alt; ?>">
    	        	<?php endif; ?>
                <span class="cd-image-label" data-type="modified">Modified</span>
              </div>

              <span class="cd-handle"></span>
            </figure>
          </div>
          <!-- Comparison Slider end -->

        </div>

      </div>
    </div>
</section>