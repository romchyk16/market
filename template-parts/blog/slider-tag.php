<?php $items = carbon_get_theme_option( 'items_slider_tag' ); ?>
<div class="header-slider">

  <?php foreach((array)$items as $item): 
    $categories = wp_get_post_categories( $item['id'], array('fields' => 'all') ); ?>

    <div class="header-slider-item">
      <h3 class="header-slider-title"><?php echo get_the_title($item['id']); ?></h3>

      <?php foreach((array)$categories as $category){ ?>

        <div class="header-slider-categories"><?php echo $category->name; ?></div>

      <?php } ?>

      <p class="header-slider-date"><?php echo get_the_date('M d, Y', $item['id']); ?></p>
    </div>

  <?php endforeach; ?> 

</div>