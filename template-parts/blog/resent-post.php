<div class="blog-head head-item-line">
    <div class="main-caption"><?php echo __('Resent posts', 'market'); ?></div>
    <div class="show-more-btn">
        <a class="show-more" href="<?php echo home_url(); ?>/blog"><?php echo __('show all', 'market'); ?></a>
    </div>
    <!-- /.show-more-wrap -->
</div>
<div class="resent-post-wrap"> 

    <?php $args = array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'suppress_filters' => false
    );
    $recent_posts = wp_get_recent_posts($args); ?> 

    <?php foreach ($recent_posts as $recent) {
        $categories = wp_get_post_categories($recent["ID"], array('fields' => 'all')); ?>

        <div class="blog-item resent-post">
            <?php if (has_post_thumbnail()):
                $image_alt = get_post_meta($recent["ID"], '_wp_attachment_image_alt', true); ?>
                <div class="item-photo-wrap">
                    <a class="item-photo scale-image" href="<?php echo get_permalink($recent["ID"]); ?>">
                        <img src="<?php echo get_the_post_thumbnail_url($recent["ID"], 'full'); ?>"
                             alt="<?php echo $image_alt; ?>">
                    </a>
                </div>
                <!-- /.item-photo-wrap -->
            <?php endif; ?>
            <div class="blog-item-text-wrap">
                <div class="blog-item-text">
                    <div class="blog-item-info">
                        <?php foreach ((array)$categories as $category) { ?>
                            <a href="<?php echo get_category_link($category->term_id); ?>" class="blog-item-categories">
                                <?php echo $category->name; ?>
                            </a>
                        <?php } ?>
                        <div class="blog-item-date"><?php echo get_the_date('M d, Y', $recent["ID"]); ?></div>
                    </div>
                    <a href="<?php echo get_permalink($recent["ID"]); ?>" class="blog-item-name">
                        <?php echo esc_attr($recent["post_title"]); ?>
                    </a>
                </div>
            </div>
        </div>

    <?php } ?>

</div>
<!-- /.resent-post-wrap -->
<!-- /.blog-head -->