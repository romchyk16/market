<?php $items = carbon_get_theme_option( 'items_slider_blog' ); ?>
<div class="header-slider">

  <?php foreach((array)$items as $item): 
    $categories = wp_get_post_categories( $item['id'], array('fields' => 'all') ); ?>

    <div class="header-slider-item">
      <h3 class="header-slider-title"><a href="<?php echo get_the_permalink($item['id']); ?>"><?php echo get_the_title($item['id']); ?></a></h3>

      <?php foreach((array)$categories as $category){ ?>

        <a href="<?php echo get_tag_link($category->term_id); ?>" class="header-slider-categories"><?php echo $category->name; ?></a>

      <?php } ?>

      <p class="header-slider-date"><?php echo get_the_date('M d, Y', $item['id']); ?></p> 
    </div>

  <?php endforeach; ?>

</div>