<!-- Features section -->
<section class="features" style="background: url(<?php echo wp_get_attachment_image_url( carbon_get_post_meta( $post->ID, 'bg_features' ), 'full' ); ?>) no-repeat top;">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h3 class="features-title"><?php echo carbon_get_post_meta( $post->ID, 'title_features' ); ?></h3>
				<p class="features-desc"><?php echo carbon_get_post_meta( $post->ID, 'desc_features' ); ?></p>
				<div class="featrues-btns">
					<a href="<?php echo carbon_get_post_meta( $post->ID, 'url_btl_l_features' ); ?>" class="influrencer-btn features-btn">
						<?php echo carbon_get_post_meta( $post->ID, 'text_btn_l_features' ); ?>
					</a>
					<a href="<?php echo carbon_get_post_meta( $post->ID, 'url_btl_r_features' ); ?>" class="advertizer-btn features-btn">
						<?php echo carbon_get_post_meta( $post->ID, 'text_btn_r_features' ); ?>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>