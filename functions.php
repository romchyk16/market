<?php
/**
 * market functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package market
 */

/**
 * Carbon Fields lib
 */
add_action( 'after_setup_theme', 'crb_load' );

function crb_load() {
    require_once( 'vendor/autoload.php' );
    \Carbon_Fields\Carbon_Fields::boot();
}
add_action( 'carbon_fields_register_fields', 'tenthouse_attach_theme_options' );

function tenthouse_attach_theme_options() {

	require get_template_directory() . '/inc/custom-fields/options.php';
    require get_template_directory() . '/inc/custom-fields/home.php';
	require get_template_directory() . '/inc/custom-fields/single.php';
    require get_template_directory() . '/inc/custom-fields/blog.php';
    require get_template_directory() . '/inc/custom-fields/widget-follow.php';
	require get_template_directory() . '/inc/custom-fields/widget-newsletter.php';
}

add_filter('carbon_fields_map_field_api_key', 'market_get_gmaps_api_key');
function market_get_gmaps_api_key($current_key){
    return 'AIzaSyDL-pHnwb1JGvfG_7wCKcOa2aDOyexT8Ws';
}

/**
 * Carbon Options WPML Support
 */
function crb_get_i18n_suffix() {
    $suffix = '';
    if ( ! defined( 'ICL_LANGUAGE_CODE' ) ) {
        return $suffix;
    }
    $suffix = '_' . ICL_LANGUAGE_CODE;
    return $suffix;
}

function crb_get_i18n_theme_option( $option_name ) {
    $suffix = crb_get_i18n_suffix();
    return carbon_get_theme_option( $option_name . $suffix );
}

/**
 * Carbon Widget init
 */
add_action( 'widgets_init', 'load_widgets' );
// add_action( 'widgets_init', 'load_newsletter_widgets' );

if (!function_exists('market_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function market_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on market, use a find and replace
         * to change 'market' to the name of your theme in all the template files.
         */
        load_theme_textdomain('market', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'header_1' => esc_html__('Header 1', 'market'),
            'footer_1' => esc_html__('Footer 1', 'market'),
            'footer_2' => esc_html__('Footer 2', 'market'),
            'footer_3' => esc_html__('Footer 3', 'market'),
            'footer_4' => esc_html__('Footer 4', 'market')
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('market_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'market_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function market_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('market_content_width', 640);
}

add_action('after_setup_theme', 'market_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function market_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'market'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'market'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'market_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function market_scripts() {
	//==================================================== CSS ====================================================//
	wp_enqueue_style( 'market-style', get_stylesheet_uri() );
	wp_enqueue_style( 'market-fonts-css', 'https://fonts.googleapis.com/css?family=Lato' );
	wp_enqueue_style( 'market-awesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
	wp_enqueue_style( 'market-bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' );
	wp_enqueue_style( 'market-slick-css', get_template_directory_uri() . '/libs/slick/slick.css' );
	wp_enqueue_style( 'market-slick-theme-css', get_template_directory_uri() . '/libs/slick/slick-theme.css' );
//	wp_enqueue_style( 'market-bootstrap-css', get_template_directory_uri() . '/libs/bootstrap/bootstrap.min.css' );
	wp_enqueue_style( 'market-fotorama-css', get_template_directory_uri() . '/css/vendors/fotorama.css' );
    wp_enqueue_style('market-fontello-css', get_template_directory_uri() . '/css/fontello/fontello.css');
	wp_enqueue_style( 'market-main-css', get_template_directory_uri() . '/css/main.css' );
	wp_enqueue_style( 'market-header-css', get_template_directory_uri() . '/css/header.css' );
	if ( !is_page_template('template-home.php') ) {
		wp_enqueue_style( 'market-blog-css', get_template_directory_uri() . '/css/blog.css' );
    }
    //==================================================== JS ====================================================//
	// wp_enqueue_script( 'market-jquery-js', 'https://code.jquery.com/jquery-3.2.1.min.js', array(), date("Ymd"), true );
	// wp_enqueue_script( 'market-popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js', array(), date("Ymd"), true );
	// wp_enqueue_script( 'market-bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array(), date("Ymd"), true );
	// wp_enqueue_script( 'market-bootstrap-js', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-matchHeight-js', get_template_directory_uri() . '/js/vendors/jquery.matchHeight.min.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-fotorama-js', get_template_directory_uri() . '/js/vendors/fotorama.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-comparison-slider-js', get_template_directory_uri() . '/js/comparison-slider.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-slickr-js', get_template_directory_uri() . '/libs/slick/slick.min.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-cookie-js', get_template_directory_uri() . '/js/jquery.cookie.js', array(), date("Ymd"), true );
    wp_enqueue_script( 'market-main-js', get_template_directory_uri() . '/js/main.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-common-js', get_template_directory_uri() . '/js/common.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-navigation', get_template_directory_uri() . '/js/navigation.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'market-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), date("Ymd"), true );
	wp_enqueue_script( 'lazyload', get_template_directory_uri() . '/libs/lazy-load/lazysizes.min.js', array(), date("Ymd"), true );

//	dd( get_template_directory_uri() . '/libs/lazy-load/lazysizes.min.js');
    wp_localize_script('market-main-js', 'params', array(
        'ajaxurl'       => admin_url('admin-ajax.php'),
        'directory_uri' => get_template_directory_uri()
    ));

    if (is_page_template('template-blog.php') || is_archive()) {
        wp_enqueue_script('market-blog-js', get_template_directory_uri() . '/js/blog.js', array(), '', true);
    }

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'market_scripts');

function market_admin_scripts()
{
    wp_enqueue_style('market-admin-css', get_template_directory_uri() . '/css/admin.css');
}

add_action('admin_enqueue_scripts', 'market_admin_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Breadcrumbs.
 */
require get_template_directory() . '/inc/breadcrumbs.php';

/**
 * Post views.
 */
require get_template_directory() . '/inc/postviews.php';

/**
 * Like/dislike.
 */
require get_template_directory() . '/inc/likes.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load Jetpack compatibility file.
 */ 
function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}
add_action('after_setup_theme', 'remove_admin_bar');

/**
 * SVG to upload mimes.
 */
function add_svg_to_upload_mimes( $upload_mimes ) {
    $upload_mimes['svg'] = 'image/svg+xml';
    $upload_mimes['svgz'] = 'image/svg+xml';
    return $upload_mimes;
}
add_filter( 'upload_mimes', 'add_svg_to_upload_mimes', 10, 1 );

/**
 * Hide editor on choose templates
 */
add_action( 'admin_head', 'hide_editor' );
function hide_editor() {
	$template_file = $template_file = basename( get_page_template() );

	if( $template_file == 'template-home.php' || $template_file == 'template-blog.php' ){
		remove_post_type_support('page', 'editor');
	}
}

/**
 * Ajax blockchain api
 */
add_action('wp_ajax_blockchain', 'ajax_blockchain');
add_action('wp_ajax_nopriv_blockchain', 'ajax_blockchain');

function ajax_blockchain(){

	$url  = 'https://blockchain.socialmedia.market/exchange/usd';
	$data = file_get_contents($url);

	wp_send_json( $data );

	wp_die();
}

/**
 * Ajax load more
 **/
add_action('wp_ajax_loadmore', 'ajax_loadmore_market');
add_action('wp_ajax_nopriv_loadmore', 'ajax_loadmore_market');

function ajax_loadmore_market(){

	$count = 1;
	$args  = array(
		'post__not_in' => $_POST['post_ids'],
		'posts_per_page' => 3,
		'post_type' 	 => 'post',
		'post_status' 	 => 'publish',
		'orderby' 		 => 'title',
		'order' 		 => 'ASC'
	);

	$query = new WP_Query($args);

	if( $query->have_posts() ) :

		while( $query->have_posts() ): $query->the_post();

			get_template_part('template-parts/blog', 'loop');

		endwhile; ?>
		<script>
		    found_posts = '<?php echo $query->found_posts; ?>';
		    post_ids = '<?php echo $_POST['post_ids']; ?>';
		</script>
	<?php endif; ?>

	<?php wp_reset_postdata();
	wp_die();
}

/**
 * Shortcode social sharing
 **/
function social_sharing(){

    extract(shortcode_atts(array(), $atts));

    $html = '<ul class="share-article">
                <li class="link-facebook">
                    <a target="_blabk" href="http://www.facebook.com/share.php?u=' . urlencode(get_the_permalink()) . '&title=' . urlencode(get_the_title()). '">
                        <i class="icon-facebook"></i>Facebook
                    </a>
                </li>
                <li class="link-twitter">
                    <a target="_blabk" href="http://twitter.com/home?status='. urlencode(get_the_title()). '+'. urlencode(get_the_permalink()) . '">
                        <i class="icon-twitter"></i>Twitter
                    </a>
                </li>
                <li class="link-gplus">
                    <a target="_blabk" href="https://plus.google.com/share?url=' . urlencode(get_the_permalink()) . '">
                        <i class="icon-gplus"></i>
                    </a>
                </li>
                <li class="link-linkedin">
                    <a target="_blabk" href="http://www.linkedin.com/shareArticle?mini=true&url=' . urlencode(get_the_permalink()) . '&title=' . urlencode(get_the_title()) . '&source=' . get_bloginfo("url") . '">
                        <i class="icon-linkedin"></i>
                    </a>
                </li>
                <li class="link-pinterest">
                    <a target="_blabk" href="https://pinterest.com/pin/create/button/?url=' . urlencode(get_the_permalink()) . '&media=' . urlencode(get_template_directory_uri()."/img/logo.png") . '&description=' . urlencode(get_the_title()). '">
                        <i class="icon-pinterest-circled"></i>
                    </a>
                </li>
            </ul>';

    return $html;
}
add_shortcode("social_sharing", "social_sharing");

/**
 * Reorder comment fields
 */
add_filter('comment_form_fields', 'market_reorder_comment_fields' );
function market_reorder_comment_fields( $fields ){

    $new_fields = array();
    $myorder    = array('author', 'email', 'comment');

    foreach( $myorder as $key ){
        $new_fields[ $key ] = $fields[ $key ];
        unset( $fields[ $key ] );
    }

    if( $fields )
        foreach( $fields as $key => $val )
            $new_fields[ $key ] = $val;

    return $new_fields;
}

/**
 * Fix redirect after commet
 */
function redirect_after_comment($location){

    return $_SERVER["HTTP_REFERER"];
}
add_filter('comment_post_redirect', 'redirect_after_comment');

/**
 * Redirect if language blog not english and not russian
 */
function redirect_to_blog(){

    if( is_page_template('template-blog.php') || is_archive() || is_single() ){

        if ( function_exists('icl_object_id') && ICL_LANGUAGE_CODE != 'ru' && ICL_LANGUAGE_CODE != 'en') {

            wp_redirect( site_url() . '/blog', 301 ); 
            exit;
        }
    }
}
add_action('wp_enqueue_scripts', 'redirect_to_blog');

/**
 * Custom template comment
 */
function market_comment( $comment, $args, $depth ) { 

        if ( 'div' === $args['style'] ) {
            $tag       = 'div';
            $add_below = 'comment';
        } else {
            $tag       = 'li';
            $add_below = 'div-comment';
        }

        $classes = ' ' . comment_class( empty( $args['has_children'] ) ? '' : 'parent', null, null, false );
        ?>

        <<?php echo $tag, $classes; ?> id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) { ?>
            <div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
        } 
        $current_user = wp_get_current_user(); ?>

        <div class="comment-author">
            <?php echo get_avatar( $comment ); ?>
        </div>

        <div class="comment-wrap">
            <div class="comment-metadata">
                <div class="comment-author-time">
                    <h4><?php echo $comment->comment_author; ?></h4>
                    <?php $comm_link = get_comment_link( $comment->comment_ID ); ?>
                    <?php $d = "M d, Y"; $comment_date = get_comment_date( $d, $comment->comment_ID ); ?>
                    <time class="blog-item-date" datetime="<?php echo $comment_date; ?>">
                        <?php echo $comment_date; ?>
                    </time>
                </div>
                <div class="comment-edit">
                    <!-- <a href="#" class="comment-delete-link">Delete </a> -->
                    <!-- <a href="#" class="comment-reply-link icon-reply">Reply</a> -->
                    <?php 
                    edit_comment_link( __( ' Edit' ), '  ', '' ); 

                    comment_reply_link(
                        array_merge(
                            $args,
                            array(
                                'add_below' => $add_below,
                                'depth'     => $depth,
                                'max_depth' => $args['max_depth']
                            )
                        )
                    ); ?>
                </div>
            </div>
            <div class="comment-content">
                <p><?php comment_text(); ?></p>
            </div>
        </div>

        <?php if ( 'div' != $args['style'] ) { ?>
            </div>
        <?php } 

}

/**
 * Rewrite category routes
 */
// Post and category has same url base, so if there is no requested category
// we manually switch request to post parameters.
function fix_category_requests($request){
    if(
        array_key_exists('category_name' , $request)
        && ! get_term_by('slug', basename($request['category_name']), 'category')
    ){
        if (! get_term_by('slug', basename($request['category_name']), 'post_tag')) {
            $request['name'] = basename($request['category_name']);
        } else {
            $request['tag'] = basename($request['category_name']);
        }
        unset($request['category_name']);
    }

    return $request;
}
add_filter('request', 'fix_category_requests');

/**
 * Breadcrumbs custom link home after
 */
function breadcrumbs_home_after( $false, $linkpatt, $sep, $ptype ){

    $qo = get_queried_object();

    if( is_single() || is_archive() ){

        $pages = get_pages(array(
            'meta_key'   => '_wp_page_template',
            'meta_value' => 'template-blog.php'
        ));

        if (isset($pages[0])) {
            $template_id = $pages[0]->ID;
            $page = get_post( $template_id );

            return sprintf( $linkpatt, get_permalink($page), $page->post_title ) . $sep;
        }  
    }

    return $false;
}
add_action('market_breadcrumbs_home_after', 'breadcrumbs_home_after', 10, 4);

/**
 * Remove the slash at the end of the url
 */
$uri = preg_replace("/\?.*/i",'', $_SERVER['REQUEST_URI']);


if (strlen($uri) > 1) { 
  if (rtrim($uri,'/') != $uri && $uri != '/wp-admin/') {

    header("HTTP/1.1 301 Moved Permanently");
    header('Location: http://'.$_SERVER['SERVER_NAME'].str_replace($uri, rtrim($uri,'/'), $_SERVER['REQUEST_URI']));
    exit();    
  }
}