$(document).ready(function () {
    $('.header-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true
    });
    $('.load-more').click(function () {
        $('.blog-item-drop').show(400);
    });
    $('.header-lang > li').hover(function () {
        $(this).children('.header-lang-drop').stop(false, true).fadeIn(300);
    }, function () {
        $(this).children('.header-lang-drop').stop(false, true).fadeOut(300);
    });
    $('.footer-lang > li').hover(function () {
        $(this).children('.footer-lang-drop').stop(false, true).fadeIn(300);
    }, function () {
        $(this).children('.footer-lang-drop').stop(false, true).fadeOut(300);
    });
    $('.toggle-nav').on('click', function () {
        $(this).toggleClass('on');
        var $this = $(this);

        if ($this.hasClass('on')) {

            $('body').addClass('overflow-scroll');
            $('.mnu').animate({
                'left': '0px',
                'width': '100%'
            }, 200);
            $('.toggle-nav').css('position', 'fixed');
            $('.toggle-nav').animate({
                'right': '25px',
                'top': '25px'
            }, 200);
        } else {
            $('body').removeClass('overflow-scroll');
            $('.mnu').animate({
                'left': '-250px',
                'width': '250px'
            }, 200);
            $('.toggle-nav').css('position', 'absolute');
        }
    });


    if ($(".heateor_sss_sharing_container")[0]) {
        var social_pos = $('.heateor_sss_sharing_container').offset().top;

        $(window).scroll(function (event) {
            var scroll = $(window).scrollTop();

            if (scroll > social_pos) {
                $('.heateor_sss_sharing_container').addClass('sticky')
            } else {
                $('.heateor_sss_sharing_container').removeClass('sticky')
            }
        });
    }


    $('.header-tooltip').appendTo('.header-nav-wrapper .list-unstyled .menu-item');
    $('.header-slider').addClass('show');
});