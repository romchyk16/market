$(document).ready(function() {

    var item = $(".blog-item");
    for(var i = 0; i < item.length; i+=3) {
        item.slice(i, i+3).wrapAll("<div class='blog-item-pair'></div>");
    }


    $('.blog-item').on('mouseenter', function () {
        var height = $(this).find('.blog-item-info').outerHeight();
        if (height > 73) {
            $(this).find('.blog-item-text').css('height', height);
        }
    });
    $('.blog-item').on('mouseleave', function () {
        $(this).find('.blog-item-text').css('height', '73px');
    });


});