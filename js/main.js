$(document).ready(function(){

    /**
     * Button for block Team show/hide
     */
    $("#btn-team").click(function() {

        $('.toggle').toggleClass('team-hide');
        $('.btn-text').toggleClass('btn-text--hide');
    });

    /**
     * Smooth scrolling
     */
    $("a.scrollto").click(function() {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 800);

        return false;
    });

    /**
     * Like/dislike.
     */
    var date = new Date();
    var days = 30;
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

    $('.lp-like').click(function(){
        var likevalue = $(this).data('like');
        $(this).html('<img class="like-preloader" src="'+params.directory_uri+'/img/loader.gif" />');

        $.ajax({
            url  : params.ajaxurl,
            type:'POST',
            data: {'action': 'likes','likes': likevalue,},
            success: function(data){
                $('#lk' + likevalue).html(data);
                var likename = 'likename_' + likevalue;
                $.cookie(likename, '1', { expires: date, path: '/' });
            },
        });
    });

    /*$('.lp-dislike').click(function(){
        var dislikevalue = $(this).data('dislike');
        $(this).html('<img src="/wp-content/themes/thrmename/images/loader.gif" />');
        $.ajax({
            url: "/wp-admin/admin-ajax.php",
            type:'POST',
            data: {'action': 'lpestimate','dislikes': dislikevalue,},
            success: function(data){
                $('#dlk' + dislikevalue).html(data);
                var likename = 'likename_' + dislikevalue;
                $.cookie(likename, '1', { expires: date, path: '/' });
            },
        });
    });*/

    /** 
     * Load more
     */
    $('.load-more').click(function() {

        var post_ids = [];
        $('.blog-item').each(function(index, el) {
            var postId = $(el).data("post-id");
            post_ids.push(postId);
        });

        data = {
            action: 'loadmore',
            post_ids: post_ids
        };

        $.ajax({
            url  : params.ajaxurl,
            data : data,
            type : 'POST',
            beforeSend: function() {
                $('.load-more').html('<img class="img-preload" src="'+params.directory_uri+'/img/load-more.gif" alt="preloader">');

            },
            complete: function() {
                $('.load-more').html('Load more');
            },
            success: function(data) {

                if (data) {
                    $(".blog-item-wrapper").append('<div class="blog-item-pair">' + data + '</div>');
                }

                $('.blog-item-pair').each(function () {
                   var length_items = $(this).find('.blog-item').length;

                    if (length_items == 2) {
                        $(this).find('.blog-item-big').css({
                            'position': 'relative',
                            'float' : 'right'
                        });
                    }
                });

                if (parseInt(found_posts) <= parseInt(4)) {
                    $('.load-more-wrapper').hide();
                    $('.blog-item-container').css({
                        'padding-bottom' : 0
                    })
                } else {
                    $('.load-more-wrapper').show();
                }
            }
        });
    });

    /** 
     * Blockchain api
     */
    data = {
        action: 'blockchain'
    };

    $.ajax({
        url  : params.ajaxurl,
        data : data,
        type : 'GET',
        beforeSend: function() {

        },
        complete: function() {

        },
        success: function(data) {

            if (data) {
                var data_obj = JSON.parse(data);
                var eth = data_obj.eth;
                var btc = data_obj.btc;
                var smt = data_obj.smt;

                $('.tp').text('$ ' + smt.rate.toFixed(3));
                $('.tpp').text('(' + smt.percent_change['24h'] + '%)');
                $('.eth').text('$ ' + eth.rate);
                $('.btc').text('$ ' + btc.rate);

            } else {
                
            }
        }
    });

    // Initial
    var fotoramaOptions = {
        maxwidth: '100%',
        width: '100%',
        ratio: 670/380,
        allowfullscreen: true,
        nav: 'dots',
        autoplay: 5000,
        auto: false
    };

    function updateFotoramaNavWrap(nav) {
        var cardFooterHeight = $('.card-video .card-video--footer').outerHeight();
        var navWrapHeight = 80;// $(nav).outerHeight();
        var margin = 10;
        $(nav).css({bottom: -(navWrapHeight + cardFooterHeight + margin)});
    }

    function initNavButtons(target) {
        var $goNext = $('.fotorama-go-next');
        var $goPrev = $('.fotorama-go-prev');

        $goNext.off('click');
        $goNext.on('click', function(event){
            $(target).data('fotorama').show('>');
            event.preventDefault();
        });

        $goPrev.off('click');
        $goPrev.on('click', function(event){
            $(target).data('fotorama').show('<');
            event.preventDefault();
        });
    }

    var cardVideoFooter = {
        title: '#card-video-title',
        desc: '#card-video-desc',
        country: '#card-video-country',
        date: '#card-video-date',
        share: '#card-video-share'
    };

    var $videoBlock = $('#video-fotorama');
    $videoBlock.on('fotorama:load, fotorama:show', function(e, fotorama, extra){
        updateVideoInfo(fotorama.activeFrame);
    }).fotorama(fotoramaOptions);

    var $fotorama = $videoBlock.data('fotorama');

    updateFotoramaNavWrap('.fotorama__nav-wrap');
    initNavButtons('#video-fotorama');

    $(window).on('resize', function() {
        updateFotoramaNavWrap('.fotorama__nav-wrap');
    });

    function updateVideoInfo(activeFrame) {
        $(cardVideoFooter.title).text(activeFrame.title || '');
        $(cardVideoFooter.desc).text(activeFrame.desc || '');
        $(cardVideoFooter.date).text(activeFrame.date || '');
        $(cardVideoFooter.country).text(activeFrame.country || '');
        $(cardVideoFooter.share).attr('href', activeFrame.share || '#');
    }

    $('.videos-list').on('click', 'a', function(event){
        var videoListData = $(this).data('video-list');
        var videoListJson = $.parseJSON(JSON.stringify(videoListData));
        if ($videoBlock) {

            // change active menu item
            $('.videos-list').find('li').removeClass('active');
            $(this).closest('li').addClass('active');

            // reload items
            $fotorama.load(videoListJson);

            // update nav wrap
            updateFotoramaNavWrap('.fotorama__nav-wrap');
        }

        event.preventDefault();
    });

    /** 
     * Init first tab
     */
    $('.videos-list').find('a:first').click();

    /**
     * Create a clone of the menu, right next to original.
     */
    // $('.heateor_sss_sharing_container').addClass('original').clone().insertAfter('.heateor_sss_sharing_container').addClass('cloned').css('position','fixed').css('top','0').css('margin-top','0').css('z-index','500').removeClass('original').hide();
    //
    // scrollIntervalID = setInterval(stickIt, 10);


    // function stickIt() {
    //
    //   var orgElementPos = $('.original').offset();
    //   orgElementTop = orgElementPos.top;
    //
    //   if ($(window).scrollTop() >= (orgElementTop)) {
    //     // scrolled past the original position; now only show the cloned, sticky element.
    //
    //     // Cloned element should always have same left position and width as original element.
    //     orgElement = $('.original');
    //     coordsOrgElement = orgElement.offset();
    //     leftOrgElement = coordsOrgElement.left;
    //     widthOrgElement = orgElement.css('width');
    //     $('.cloned').css('left',leftOrgElement+'px').css('top',0).css('width',widthOrgElement).show();
    //     $('.original').css('visibility','hidden');
    //   } else {
    //     // not scrolled past the menu; only show the original menu.
    //     $('.cloned').hide();
    //     $('.original').css('visibility','visible');
    //   }
    // }



    /** 
     * Replase tag <h2> on <div> for widgets
     */
    var cat_text = $(".widget_categories .widget-title").text();
    var tag_text = $(".widget_tag_cloud .widget-title").text();
    $(".widget_categories .widget-title").replaceWith("<div>"+cat_text+"</div>");
    $(".widget_tag_cloud .widget-title").replaceWith("<div>"+tag_text+"</div>");

});