<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package market
 */
$count = 1;

get_header(); ?>

<!-- Blog section -->
<section class="blog">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-width">
				<div class="blog-head">
					<?php market_breadcrumbs(); ?>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-9">
                <div class="blog-item-wrapper">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">
						<?php
						the_archive_title( '<h1 class="page-title">', '</h1>' );
						the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</header><!-- .page-header --><br><br>

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						get_template_part('template-parts/blog', 'loop');

					endwhile;

				else :

					get_template_part( 'template-parts/content', 'none' );

				endif;
				?>

				</div>
            </div>
			<div class="col-md-3">
				<?php get_template_part('template-parts/sidebar'); ?>
			</div>
		</div>
	</div>
</section>		

<?php get_footer();