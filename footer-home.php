<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Market
 */
?>
	<!-- Section: Footer -->
	  <footer class="section-footer py-5">
	    <div class="container">
	      <div class="row">
	        <div class="col-12 col-lg-2 text-center text-lg-left order-last order-lg-first">

	        	<?php echo market_switcher_languages('footer'); ?>

	        </div>

	        <div class="col-6 col-md-3 col-lg-2 mb-4 mb-lg-0">
	          <h5 class="mb-4">
	          	<?php if(!carbon_get_theme_option('title_menu_1_footer')){ echo '&nbsp;'; }else{ echo carbon_get_theme_option('title_menu_1_footer'); } ?>
	          </h5>
			  <?php if( has_nav_menu('footer_1') ){ 
					wp_nav_menu( array(
		                'theme_location' => 'footer_1',
		                'container'      => false,
		                'items_wrap'     => '<ul class="list-unstyled">%3$s</ul>',
		            ) ); 
		      } ?>
	        </div>

	        <div class="col-6 col-md-3 col-lg-2 mb-4 mb-lg-0">
	          <h5 class="mb-4">
	          	<?php if(!carbon_get_theme_option('title_menu_2_footer')){ echo '&nbsp;'; }else{ echo carbon_get_theme_option('title_menu_2_footer'); } ?>
	          </h5>
	          <?php if( has_nav_menu('footer_2') ){ 
					wp_nav_menu( array(
		                'theme_location' => 'footer_2',
		                'container'      => false,
		                'items_wrap'     => '<ul class="list-unstyled">%3$s</ul>',
		            ) ); 
		      } ?>
	        </div>

	        <div class="col-6 col-md-3 col-lg-2 mb-4 mb-lg-0">
	          <h5 class="mb-4">
	          	<?php if(!carbon_get_theme_option('title_menu_3_footer')){ echo '&nbsp;'; }else{ echo carbon_get_theme_option('title_menu_3_footer'); } ?>
	          </h5>
	          <?php if( has_nav_menu('footer_3') ){ 
					wp_nav_menu( array(
		                'theme_location' => 'footer_3',
		                'container'      => false,
		                'items_wrap'     => '<ul class="list-unstyled">%3$s</ul>',
		            ) ); 
		      } ?>
	        </div>

	        <div class="col-6 col-md-3 col-lg-2 mb-4 mb-lg-0">
	          <h5 class="mb-4">
	          	<?php if(!carbon_get_theme_option('title_menu_4_footer')){ echo '&nbsp;'; }else{ echo carbon_get_theme_option('title_menu_4_footer'); } ?>
	          </h5>
	          <?php if( has_nav_menu('footer_4') ){ 
					wp_nav_menu( array(
		                'theme_location' => 'footer_4',
		                'container'      => false,
		                'items_wrap'     => '<ul class="list-unstyled">%3$s</ul>',
		            ) ); 
		      } ?>
	        </div>

	        <div class="col-12 col-lg-2 mb-4">
	          <!-- Social links starts -->
			  <?php $icons = carbon_get_theme_option('rep_social_footer'); $count = 1; ?>
	          <div class="social-links">
				<div class="row no-gutters text-center">

	          	<?php foreach($icons as $icon): ?>

					<?php if ($icon['url_rep_social_footer'] && $icon['icons_rep_social_footer']['class']):  ?>

			            <div class="col mb-2">
			                <a target="_blank" href="<?php echo $icon['url_rep_social_footer']; ?>">
			                	<i class="<?php echo $icon['icons_rep_social_footer']['class']; ?>" aria-hidden="true"></i>
			                </a>
			            </div>

			            <?php if($count % 5 == 0): ?>
							<div class="w-100"></div>
		            	<?php endif; ?>

		            <?php endif; 
					++$count; 
				endforeach; ?>

	            </div>
	          </div>
	          <!-- Social links ends -->
	        </div>
	      </div>

	    </div>

	  </footer>

	  <!-- Section: Footer Copyright-->
	  <section class="section-copyrights">
	    <div class="container py-4">
	      <div class="row align-items-center">
	        <div class="col-lg-3 text-center text-lg-left mb-4 mb-lg-0">
	          <a href="<?php echo esc_url(home_url('/')); ?>" class="logo-footer" title="<?php echo carbon_get_theme_option('text_logo_footer'); ?>" target="_blank" style="background: transparent url(<?php echo wp_get_attachment_image_url( carbon_get_theme_option('logo_footer'), 'full' ); ?>) top center no-repeat;">
	            <span class="sr-only"><?php echo carbon_get_theme_option('text_logo_footer'); ?></span>
	          </a>
	        </div>
	        <div class="col-lg-6 text-center ">
	          <p class="font-weight-light mb-1"><?php echo carbon_get_theme_option('copyright_footer'); ?></p>
	          <ul class="list-inline font-weight-light mb-lg-0">
	            <li class="list-inline-item px-1">
					<?php if(!empty(carbon_get_theme_option('polici_footer')) && !empty(carbon_get_theme_option('text_polici_footer'))): ?>
		              <a href="<?php echo carbon_get_theme_option('polici_footer'); ?>" target="_blank">
		                <u><?php echo carbon_get_theme_option('text_polici_footer'); ?></u>
		              </a>
					<?php endif; ?>
	            </li>
	            <li class="list-inline-item px-1">
	            	<?php if(!empty(carbon_get_theme_option('terms_footer')) && !empty(carbon_get_theme_option('text_terms_footer'))): ?>
		              <a href="<?php echo carbon_get_theme_option('terms_footer'); ?>" target="_blank">
		                <u><?php echo carbon_get_theme_option('text_terms_footer'); ?></u>
		              </a>
					<?php endif; ?>
	            </li>
	          </ul>
	        </div>
	        <div class="col-lg-3 text-center text-lg-right">
	          <p class="font-weight-light m-0">
	          	<?php if(!empty(carbon_get_theme_option('mail_footer'))): ?>
	            	<a href="mailto:<?php echo carbon_get_theme_option('mail_footer'); ?>">
	            		<?php echo carbon_get_theme_option('mail_footer'); ?>	
	            	</a>
            	<?php endif; ?>
	          </p>
	        </div>
	      </div>
	    </div>
	  </section>
</div>

	</main>
	<!-- scripts -->
	<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/vendors/jquery.matchHeight.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/vendors/fotorama.js"></script>
	<!-- <script src="<?php //echo get_template_directory_uri(); ?>/js/main.js"></script> -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/comparison-slider.js"></script>
	<!-- blog -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
	<script src="<?php echo get_template_directory_uri(); ?>/libs/slick/slick.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>

	<?php wp_footer(); ?>
</body>
</html>