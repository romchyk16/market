<?php
/**
 * Template name: Home
 */

get_header(); ?>

<?php if (have_posts()) : ?>

    <?php while (have_posts()) : the_post(); ?>

        <?php get_template_part('template-parts/nav'); ?>

        <?php get_template_part('template-parts/home/main'); ?>

        <?php get_template_part('template-parts/home/trusted'); ?>

        <?php get_template_part('template-parts/home/benefits'); ?>

<!--        --><?php //get_template_part('template-parts/home/exchange'); ?>

        <?php get_template_part('template-parts/home/team'); ?>

        <?php get_template_part('template-parts/home/videos'); ?>

        <?php get_template_part('template-parts/home/books'); ?>

    <?php endwhile; ?>

<?php endif; ?>

<?php get_footer();